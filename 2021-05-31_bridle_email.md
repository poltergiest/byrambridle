

# 2021-05-31 Bridle E-Mail

On 2021-05-31, Bridle sent this e-mail to an undisclosed number of recipients.

```
Dear recipient,

You are one of hundreds of people that have been blind carbon copied on this e-mail. Last Thursday evening I was interviewed on radio about COVID-19 vaccines for children and adolescents (https://omny.fm/shows/on-point-with-alex-pierson/new-peer-reviewed-study-on-covid-19-vaccines-sugge). This interview went viral around the world. Although I received hundreds of supportive e-mails and phone calls from around the globe, a vicious smear campaign has been initiated against me. This included the creation of a libelous website using my domain name. Such are the times that an academic public servant can no longer answer people's legitimate questions with honesty and based on science without fear of being harassed and intimidated.

However, it is not in my nature to allow scientific facts to be hidden from the public. I have attached a brief report that outlines the key science in support of what I said. This was written with my colleagues in the Canadian COVID Care Alliance (CCCA). We are a group of independent Canadian doctors, scientists, and professionals aiming to provide top quality, evidence-based information about COVID-19, intent on reducing hospitalizations and saving more lives. Our goal is to provide you with unbiased, peer-reviewed science that is relevant for you so that you can stay on the leading edge of the ever-evolving data, while at the same time focussing all of your efforts on your wellbeing, or, if you are a medical practitioner, the wellbeing of your patients.

Please feel free to send the attached brief report to as many people as possible. It is a very important message to get out to all Canadians.  

The Canadian COVID Care Alliance is drafting a more extensive document that will dive into broader and deeper details about issues related to COVID-19 vaccines and youth. If you are interested in receiving this full article when it is ready, please sign-up to our e-mail distribution list at https://mailchi.mp/5666d252288c/canadian-covid-care-alliance. Please do not e-mail me directly for this request since my inbox is currently unmanageable.

Because I care about our children,
Byram

___________________________________________________

Byram W. Bridle, PhD
Associate Professor of Viral Immunology
Office Room #4834
Lab Room #3808
Building #89 (NW corner Gordon/McGilvray)
Department of Pathobiology
University of Guelph
50 Stone Road East
Guelph, Ontario, Canada
N1G 2W1
Office Telephone #519-824-4120 x54657
Lab Telephone #519-824-4120 x53616
E-mail: bbridle@uoguelph.ca
https://ovc.uoguelph.ca/pathobiology/people/faculty/Byram-W-Bridle
```