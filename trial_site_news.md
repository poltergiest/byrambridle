

# Trial Site News


The [about us page](https://web.archive.org/save/https://trialsitenews.com/about-us/) contains some familiar names:

## Members

Management

- Daniel O’Connor
  - Also does https://www.acresglobal.net/about-us/our-team/
- Mark Hadfield
- Dr. Tom Pitts
- Dr. Erin Stair
  - Her main site is https://bloomingwellness.com/ where they sell a "ZENBand"
  - Interview with Malone: https://www.bitchute.com/video/w7bI1tUgjGoR/
- Shabnam P. Mohamed

Advisory Committee

- Dr. Peter McCullough
- Dr. Robert Malone
- Bart Reijs



https://trialsitenews.com/should-you-get-vaccinated/





## Address

What about the address listed?

```
159 W Broadway, Suite 200
Salt Lake City, UT 84101
```

As it turns out, that's just [virtual office space](https://web.archive.org/web/20210616162022/https://www.davincivirtual.com/loc/us/utah/salt-lake-city-virtual-offices/facility-2381).

### Others at address

DARIN ASHTON PRINCIPE at MindWorks
https://opennpi.com/provider/1619390218
https://opennpi.com/provider/1538667415


https://betterboundaries.org/faq/
