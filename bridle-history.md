

# History of Byram Bridle's Claims

## Contents

- [History of Byram Bridle's Claims](#history-of-byram-bridles-claims)
  - [Contents](#contents)
  - [Bridle's Vaccine](#bridles-vaccine)
    - [Announcement](#announcement)
    - [Grant](#grant)
  - [Bridle's Many Concerns](#bridles-many-concerns)
    - [2020-06: Concerns about scientists making promises about vaccine timelines](#2020-06-concerns-about-scientists-making-promises-about-vaccine-timelines)
    - [2020-07: Concerns about the elderly responding to the vaccine](#2020-07-concerns-about-the-elderly-responding-to-the-vaccine)
    - [2020-10: Concerns we need "high-quality COVID-19 vaccine" and "lack of reviews"](#2020-10-concerns-we-need-high-quality-covid-19-vaccine-and-lack-of-reviews)
    - [2021-02: Outstanding questions about the emergency use of vaccines](#2021-02-outstanding-questions-about-the-emergency-use-of-vaccines)
      - [1. Long-term safety profile of COVID-19 vaccines](#1-long-term-safety-profile-of-covid-19-vaccines)
      - [2. Duration of immunity of COVID-19 vaccines](#2-duration-of-immunity-of-covid-19-vaccines)
      - [3. Effectiveness of COVID-19 vaccines](#3-effectiveness-of-covid-19-vaccines)
      - [4. Risk of variants that can evade vaccine-induced immunity](#4-risk-of-variants-that-can-evade-vaccine-induced-immunity)
      - [5. Untested COVID-19 vaccine regimens](#5-untested-covid-19-vaccine-regimens)
    - [2021-03: Concerns about childcare guidance](#2021-03-concerns-about-childcare-guidance)
    - [2021-03: Concerns about the risks of lockdowns causing autoimmune diseases in children](#2021-03-concerns-about-the-risks-of-lockdowns-causing-autoimmune-diseases-in-children)
    - [2021-03: Concerns about the dosing interval](#2021-03-concerns-about-the-dosing-interval)
    - [2021-05: Concerns about vaccines in kids](#2021-05-concerns-about-vaccines-in-kids)
  - [Media Appearances with Covid Plan B](#media-appearances-with-covid-plan-b)
  - [Other Media](#other-media)
  - [Bridle Acts as an Expert Witness for Adamson Barbecue](#bridle-acts-as-an-expert-witness-for-adamson-barbecue)
    - [Contents](#contents-1)
    - [Excerpts](#excerpts)
      - [1. The Problem](#1-the-problem)
      - [3. SARS-CoV-2: A Virus that Follows Typical Population Dynamics](#3-sars-cov-2-a-virus-that-follows-typical-population-dynamics)
      - [4. SARS-CoV-2 is Not a Problem of Pandemic Proportions](#4-sars-cov-2-is-not-a-problem-of-pandemic-proportions)
      - [5. Results of PCR Tests to Detect SARS-CoV-2 Must be Interpreted with Caution](#5-results-of-pcr-tests-to-detect-sars-cov-2-must-be-interpreted-with-caution)
      - [6. Asymptomatic Transmission of SARS-CoV-2 is Negligible](#6-asymptomatic-transmission-of-sars-cov-2-is-negligible)
      - [7. Individuals Who Had COVID-19 Cannot Re-Transmit the Virus.](#7-individuals-who-had-covid-19-cannot-re-transmit-the-virus)
      - [8. SARS-CoV-2 Variants of Concern](#8-sars-cov-2-variants-of-concern)
      - [9.  Masking Lacks Rationale in the Context of SARS-CoV-2 Spreading via Aerosols](#9--masking-lacks-rationale-in-the-context-of-sars-cov-2-spreading-via-aerosols)
      - [10. Prolonged Isolation and Masking of Children Can Cause Irreparable Harm to Their Immune Systems](#10-prolonged-isolation-and-masking-of-children-can-cause-irreparable-harm-to-their-immune-systems)
      - [11. Early Treatment Options that Represent Reasonable Alternatives that Would Preclude the Enactment of Emergency Orders and the Emergency Use Authorization of Experimental Vaccines](#11-early-treatment-options-that-represent-reasonable-alternatives-that-would-preclude-the-enactment-of-emergency-orders-and-the-emergency-use-authorization-of-experimental-vaccines)


## Bridle's Vaccine

Bridle is currently developing a COVID-19 vaccine using the SARS-COV-2 spike protein. He recieved $230,000 in provincial funding from the Ford Government as part of the [Ontario COVID-19 Rapid Research Fund](https://web.archive.org/web/2/https://www.ontario.ca/page/ontario-covid-19-rapid-research-fund),

### Announcement

- 2020-05-21 [CTV News Kitchener](https://web.archive.org/web/2/https://kitchener.ctvnews.ca/ontario-gives-230k-in-vaccine-research-funding-to-university-of-guelph-1.4948998) *Ontario gives $230K in vaccine research funding to University of Guelph*
- 2020-05-21 [Toronto Star](https://web.archive.org/web/20200624042634/https://www.thestar.com/politics/provincial/2020/05/21/researchers-get-7-million-in-covid-19-funding-from-ford-government.html) *Researchers get $7 million in COVID-19 funding from Ford government*
- 2020-05-21 [CBC](https://web.archive.org/web/2/https://www.cbc.ca/news/canada/kitchener-waterloo/university-guelph-vaccine-study-covid-19-1.5578787) *Guelph-led COVID-19 vaccine study gets provincial funding to move forward*
- 2020-05-21 [OVC](https://web.archive.org/web/20200809144650/https://ovc.uoguelph.ca/news/covid-19-vaccine-research-u-g-awarded-provincial-funding) *COVID-19 Vaccine Research at U of G Awarded Provincial Funding*
- 2020-05-22 [Global News](https://web.archive.org/web/2/https://globalnews.ca/news/6973373/guelph-covid19-vaccine/) *University of Guelph’s COVID-19 vaccine research receives $230,000*
- 2021-03-11 [UofG News](https://web.archive.org/web/20210324180111/https://news.uoguelph.ca/2021/03/highlights-of-how-u-of-g-researchers-have-responded-to-the-pandemic/) *Highlights of How U of G Researchers Have Responded to the Pandemic*

### Grant

The grant is described by the Government of Ontario [here](https://web.archive.org/web/20210531023606if_/https://news.ontario.ca/en/backgrounder/56989/ontario-announces-first-phase-of-research-projects-to-fight-covid-19).

**Developing Prophylactic Virus-Vectored Vaccines for COVID-19
Byram Bridle, Leonardo Susta and Sarah Wootton (Co-Principal Investigators, University of Guelph); Darwyn Kobasa, National Microbiology Laboratory, Public Health Agency of Canada (Collaborator) University of Guelph**

This research aims to develop a vaccination strategy for COVID-19. By developing avian avulavirus (AAvV-1) and adenovirus viral-vectored vaccines expressing the SARS-CoV-2 spike protein as a target antigen, researchers will test these vaccines in mice to identify a way to induce robust protective mucosal (respiratory, gastrointestinal and urogenital tract) and systemic immunity. Mucosal immunity plays a significant role in preventing pathogens from getting into the body. Systemic immunity clears any pathogens that bypass mucosal barriers. After optimization, these vaccines will be evaluated in a hamster challenge model at the National Microbiology Laboratory in Winnipeg.





## Bridle's Many Concerns

### 2020-06: Concerns about scientists making promises about vaccine timelines

- 2020-06-15 original article in [The Conversation](https://web.archive.org/web/20210321204655/https://theconversation.com/fast-covid-19-vaccine-timelines-are-unrealistic-and-put-the-integrity-of-scientists-at-risk-139824): *Fast COVID-19 vaccine timelines are unrealistic and put the integrity of scientists at risk*
- 2020-06-20 a special commentary in [Global News](https://web.archive.org/web/20210227115409/https://globalnews.ca/news/7070536/covid-19-vaccine/): *Think a COVID-19 vaccine is months away? It could actually take years*
- 2020-06-21 a video in [Global News](https://globalnews.ca/video/7088465/short-timelines-for-coronavirus-vaccine-are-giving-people-false-hope-bridle): *Short timelines for coronavirus vaccine are giving people ‘false hope,’ :Bridle*


These comments were diseminated widely, and were featured in this 2020-06-30 [Global News piece](https://web.archive.org/web/20210227091115/https://globalnews.ca/news/7119793/coronavirus-2-years-vaccine-best-case-scenario/) *2 years of coronavirus pandemic is ‘best-case scenario,’ doctor warns*.

These comments were also used in justification of not aiming for elimination of the pandemic in this 2020-09-09 [BMJ article](https://doi.org/10.1136/bmj.m3410): *Should countries aim for elimination in the covid-19 pandemic?*.

This information was also used by Covid Plan-B on 2020-08-16 in a [press release](https://web.archive.org/web/20210423032938/https://www.scoop.co.nz/stories/GE2008/S00084/viral-immunologist-says-a-vaccine-in-nz-will-be-late-limited-and-last.htm) *Viral Immunologist Says A Vaccine In NZ Will Be 'Late, Limited And Last'*



### 2020-07: Concerns about the elderly responding to the vaccine

- 2020-07-20 original article in [The Conversation](https://web.archive.org/web/20210525194740/https://theconversation.com/why-vaccines-are-less-effective-in-the-elderly-and-what-it-means-for-covid-19-141971): *Why vaccines are less effective in the elderly, and what it means for COVID-19*
- 2020-08-04 republished in [CityNews Ottawa](https://web.archive.org/web/2/https://ottawa.citynews.ca/coronavirus-covid-19-national-news/beyond-local-why-vaccines-are-less-effective-in-the-elderly-and-what-it-means-for-covid-19-2589686): *Why vaccines are less effective in the elderly, and what it means for COVID-19*.
- 2020-07-24 republished in [National Interest](https://web.archive.org/web/20210425221328/https://nationalinterest.org/blog/reboot/coronavirus-vaccine-and-seniors-could-there-be-problem-165360): *Coronavirus Vaccine and Seniors: Could There Be a Problem?*
- 2020-07-17 opinion included in [National Geographic](https://web.archive.org/web/20210301213103/https://www.nationalgeographic.com/science/article/why-those-most-risk-coronavirus-least-likely-respond-to-vaccine-cvd): *Why those most at risk of COVID-19 are least likely to respond to a vaccine*

> Here is an interesting exercise for people reading this article: find as many original research articles as you can on the topic of vaccine development that have used animal models (it could be for any disease). Then look in the subsection of the “materials and methods” section and check the age of the animals. We were shocked by what we found.  

> For the COVID-19 pandemic, it is too late to go back and build these considerations into preclinical testing. However, it is imperative that researchers still in the preclinical phase incorporate head-to-head testing of their vaccine candidates in young versus aged animals and develop strategies to optimize them in the latter.

- 2021-04-13 A video interview with "Supervisor Jim Desmond" (apparently a San Diego politician) entitled *A Conversation with Dr. Byram Bridle, University of Guelph* [live link](https://www.youtube.com/watch?v=f1WLkbd6RZY)



### 2020-10: Concerns we need "high-quality COVID-19 vaccine" and "lack of reviews"

- 2020-10-07 original article in [The Conversation](https://web.archive.org/web/20201201233525/https://theconversation.com/training-our-immune-systems-why-we-should-insist-on-a-high-quality-covid-19-vaccine-146650) *Training our immune systems: Why we should insist on a high-quality COVID-19 vaccine*
- 2020-11-19 explored in [UoG News](https://web.archive.org/web/20210311120824/https://news.uoguelph.ca/2020/11/u-of-g-viral-immunologist-has-concerns-over-new-covid-19-vaccine-candidates/) *U of G Immunologist Has Concerns Over COVID-19 Vaccine Candidates*
- 2020-11-19 story provided by UoG to [Guelph Today](https://web.archive.org/web/20201119194014/https://www.guelphtoday.com/coronavirus-covid-19-local-news/u-of-g-expert-has-concerns-over-lack-of-data-provided-about-covid-vaccines-2893268): *U of G expert has concerns over lack of data provided about COVID vaccines*

> But they should be cautious and ensure that these vaccines do not train our immune system for a response that is not optimal.

> Like an athlete, we need to avoid training our immune system in a way that contradicts the end goal. For long-term health, we must ensure that our immune systems are trained in a way that will allow us to respond most effectively against future highly pathogenic coronaviruses, some of which may prove to be more dangerous than the current one.

On 2020-12-15, Bridle explained in this article [Lack of reviews of COVID vaccine raises concern with U of G expert -- 'I don’t feel 100 per cent comfortable with it,' says immunologist Byram Bridle](https://web.archive.org/web/20201217015104/https://www.guelphtoday.com/coronavirus-covid-19-local-news/lack-of-reviews-of-covid-vaccine-raises-concern-with-u-of-g-expert-3184264) that:

> “I’m not judging the vaccines in any way, shape, or form. I’m just trying to state the facts,” says Bridle.

> “I have been asked many times personally would I take the vaccine right now? Personally, I don’t feel 100 percent comfortable with it,” says Bridle, adding that historically there have been side effects from vaccines despite taking some time to manifest.

> “If there are any chronic issues with this, we’re not going to be aware.”

> Regardless, Bridle gives the analogy of two toys priced the same but from different companies sitting on a shelf. One has a high-quality control system in place while the other has little to no quality control.

This is an interesting analogy. If the current vaccines have "little to no quality control", which vaccines have "high-quality control sustems"? Is he referring to the vaccine he is designing?

> He gives the example of the two individuals who suffered an anaphylactic reaction in the UK the very first day the Pfizer vaccine rolled out.

> “That’s only two people out of however many that were vaccinated that day,” says Bridle adding that once the vaccine is administered across the globe, it's possible hundreds of thousands of people have an anaphylactic reaction or something else.

> “I personally cannot assure anybody 100 per cent that there are going to be no issues.”




### 2021-02: Outstanding questions about the emergency use of vaccines

- 2021-02-10 original article in [The Conversation](https://web.archive.org/web/20210401151152/https://theconversation.com/5-factors-that-could-dictate-the-success-or-failure-of-the-covid-19-vaccine-rollout-152856): *5 factors that could dictate the success or failure of the COVID-19 vaccine rollout*
- 2021-02-11 republished in [UoG News](https://web.archive.org/web/20210225134907/https://news.uoguelph.ca/2021/02/5-factors-that-could-dictate-the-success-or-failure-of-the-covid-19-vaccine-rollout/): *5 Factors That Could Dictate the Success or Failure of the COVID-19 Vaccine Rollout*
- 2021-03-09 republished in [Orillia Matters](https://web.archive.org/web/20210310020615/https://www.orilliamatters.com/around-ontario/canada-factors-that-could-dictate-the-success-or-failure-of-the-covid-19-vaccine-rollout-3452855): *CANADA: Factors that could dictate the success or failure of the COVID-19 vaccine rollout*


#### 1. Long-term safety profile of COVID-19 vaccines

> Twenty-three frail elderly individuals in Norway died shortly after receiving the Pfizer vaccine. It is difficult to ascertain the reason for these deaths and they may have had nothing to do with the vaccine. It has put pressure on physicians in that country to try to determine which members of this demographic at high risk for COVID-19 mortality should and should not be vaccinated.

> If too many unpredicted severe long-term side-effects were to accrue over time, this could be cause for withdrawal of approval for a vaccine.


#### 2. Duration of immunity of COVID-19 vaccines

> For previous vaccines, we could have reasonable confidence that immunity would last at least a few years prior to public rollouts. COVID-19 vaccines only have a few months’ worth of data on duration of immunity.


#### 3. Effectiveness of COVID-19 vaccines

> Re-analysis of the data with this new information accounted for was performed by the associate editor of the British Medical Journal, who reported his non-peer-reviewed findings in the journal’s opinion column. His estimate suggests the true effectiveness of the vaccine might be as low as 19 to 29 per cent. This can’t be confirmed or refuted until raw data not included in the FDA report are released.

Bridle references this [BMJ article](https://web.archive.org/web/20210225135250/https://blogs.bmj.com/bmj/2021/01/04/peter-doshi-pfizer-and-modernas-95-effective-vaccines-we-need-more-details-and-the-raw-data/) which includes the following note:

> Five weeks ago, when I raised questions about the results of Pfizer’s and Moderna’s covid-19 vaccine trials, all that was in the public domain were the study protocols and a few press releases. Today, two journal publications and around 400 pages of summary data are available in the form of multiple reports presented by and to the FDA prior to the agency’s emergency authorization of each company’s mRNA vaccine.

So the public have aggregate data available, and all relevant health agencies have the "raw data". Just not this author or Bridle. Additionally, a "clarification" was posted [here](https://web.archive.org/web/20210304155024/https://blogs.bmj.com/bmj/2021/02/05/clarification-pfizer-and-modernas-95-effective-vaccines-we-need-more-details-and-the-raw-data/) five days before Bridle's original article was published. It included this note:

> Second, in reference to the final section of my article, readers have commented that the European Medicines Agency (EMA) is not likely to release individual participant datasets from covid-19 vaccine trials. I agree with this comment and did not intend to suggest that the EMA would do this, for the simple reason that the EMA does not itself routinely receive individual participant data from industry, so the agency has none to release.


#### 4. Risk of variants that can evade vaccine-induced immunity

> Although the risk of mutations that can evade vaccine-induced immunity cannot be accurately quantified, the way COVID-19 vaccines are being rolled out will likely increase the potential for this to occur for at least two reasons. First, the current vaccines confer narrowly focused immunity that targets a single viral spike protein.

> Secondly, the vaccination program is being rolled out in piece-meal fashion. This slow expansion of narrowly focused immunity among people who are surrounded by others who are not immune provides the time and contact with a “reservoir population” that a virus would need to generate random variants that can probe their potential to infect vaccinated people.

> Importantly, acquisition of natural immunity, which targets multiple components of the virus, may reduce the risk of re-infection with variants that can bypass spike protein-specific immunity.

One might ask how allowing infections to spread would reduce the opportunity for mutation...


#### 5. Untested COVID-19 vaccine regimens

This section describes how data on the results of mixing vaccines is not yet available. Canada is not mixing vaccines.



### 2021-03: Concerns about childcare guidance

In February 2021, the Toronto Sun ran an arc of inflammatory stories to harness outrage towards public health guidance:

- 2021-02-27: [Experts call Peel guidelines to place children in solitary quarantine 'cruel punishment'](https://web.archive.org/web/20210429031535/https://torontosun.com/news/provincial/experts-call-peel-guidelines-to-place-children-in-solitary-quarantine-cruel-punishment)
- 2021-02-28: [Peel Health child isolation policy a 'mistake' that's being fixed, says Brampton mayor](https://web.archive.org/web/20210309233920/https://torontosun.com/news/provincial/peel-health-child-isolation-policy-a-mistake-thats-being-fixed-says-brampton-mayor)
- 2021-03-01: [Ontario backs isolation of potentially exposed children, but says 'common sense' needed](https://web.archive.org/web/20210422030909/https://torontosun.com/news/provincial/ontario-backs-isolation-of-potentially-exposed-children-but-says-common-sense-needed)
- 2021-03-03: [Toronto Public Health moderates child isolation guidelines](https://web.archive.org/web/20210303085204/https://torontosun.com/news/local-news/toronto-public-health-moderates-child-isolation-guidelines)

Approximately three weeks later, the Toronto Sun published the same faux outrage from Bridle, and this was later echoed in more subdued terms in the Toronto Star:

- 2021-03-22: [Viral immunologist speaks out against 'abusive' child-quarantine policies](https://web.archive.org/web/20210331123343/https://torontosun.com/news/provincial/viral-immunologist-speaks-out-against-abusive-child-quarantine-policies)
- 2021-03-30 ['It is essentially akin to solitary confinement': UofG viral immunologist frustrated by child COVID-19 quarantine messaging](https://web.archive.org/web/20210531053044/https://www.thestar.com/local-guelph/news/2021/03/30/it-is-essentially-akin-to-solitary-confinement-uofg-viral-immunologist-frustrated-by-child-covid-19-quarantine-messaging.html?li_source=LI&li_medium=star_web_ymbii)

> Bridle wants Ontario parents to know that the science does not support these excessive policies. “As a scientist, I follow the facts,” said Bridle. “The evidence is overwhelming: there is no question now that COVID-19 is no worse than the annual flu for our elementary school children.”

> “As a scientist, I feel it’s important to relay facts to make the most informed decisions,” Bridle told the Sun. “Because what I do find is that left to the public health officials and government alone, the facts coming out from those sources aren’t 100% balanced. I feel that they are consistently imbalanced.”



### 2021-03: Concerns about the risks of lockdowns causing autoimmune diseases in children

- 2021-03-09 original article in [The Conversation](https://web.archive.org/web/20210504180746/https://theconversation.com/a-year-of-covid-19-lockdown-is-putting-kids-at-risk-of-allergies-asthma-and-autoimmune-diseases-155102): *A year of COVID-19 lockdown is putting kids at risk of allergies, asthma and autoimmune diseases*
- 2021-03-10 re-published in the [National Post](https://nationalpost.com/pmn/news-pmn/a-year-of-covid-19-lockdown-is-putting-kids-at-risk-of-allergies-asthma-and-autoimmune-diseases): *A year of COVID-19 lockdown is putting kids at risk of allergies, asthma and autoimmune diseases*
- 2021-03-11 basis for [CTV News Kitchener](https://web.archive.org/web/2/https://kitchener.ctvnews.ca/lockdown-measures-could-impact-children-s-immune-systems-1.5344205): *Lockdown measures could impact children's immune systems*
- 2021-03-17 basis for [Guelph Today](https://web.archive.org/web/20210319150725/https://www.guelphtoday.com/around-ontario/covid-19-isolation-increases-risk-of-immunological-disorders-immunologist-says-3546013): *COVID-19: Isolation increases risk of immunological disorders, immunologist says; Deprivation from ‘microbial world’ said to increase risk for ‘dysregulated immune system’ in developing youth*


> "The lockdown and restriction policies that have been enacted to help prevent the spread of COVID-19 contradict the recommendations to ensure proper immunological development in children."

> 'An unfortunate and under-appreciated long-term legacy of this pandemic will likely be a cluster of “pandemic youth” that grow up to suffer higher-than-average rates of allergies, asthma and autoimmune diseases. This will hold true for children in all countries that enacted isolation policies.'

> When asked what the solution to this could be, Bridle said he would have liked to have seen children back in school with “some return to normalcy” much sooner than established. While he admitted there’d likely be more confirmed cases in our schools, Bridle said the term of cases is typically overused as a metric – as most cases occurring in school-aged children are mild.

> “And yet, we’re essentially holding them hostage for the sake of protecting a relatively limited number of people who are at high risk of developing severe and potentially lethal disease,” said Bridle. “This is where we have to balance the decision making – I’m not going to be the person to solve this overnight, but we need parents to be fully informed and we need policy makers to be fully informed so they can make the best possible decisions for our children – and I’m not convinced it’s keeping them locked up.”



### 2021-03: Concerns about the dosing interval

- [U of G scientists concerned about extended interval between COVID-19 vaccine doses](https://web.archive.org/web/20210325143234/https://www.wellingtonadvertiser.com/u-of-g-scientists-concerned-about-extended-interval-between-covid-19-vaccine-doses/)
  - 2021-03-24, Wellington Advertiser, original article

> It’s not that there’s anything wrong with the vaccines, said Dr. Byram Bridle, a viral immunologist in the department of pathology.

> “We very much promote vaccination,” he said in an interview on March 18.

> There’s all kinds of wrong with that, Bridle said, including that the numbers used were too small to be statistically significant and that the entire claim “comes from extrapolation of data that was never designed for this type of analysis.

> Anti-vaxxers will never be convinced, but Bridle’s biggest fear is that those who are vaccine hesitant may decide not to get the vaccine because of the conflicting information.

> “We don’t want people losing faith in vaccines,” he said.



### 2021-05: Concerns about vaccines in kids

- 2021-05-28 article in the [Toronto Sun](https://web.archive.org/web/20210531055507/https://torontosun.com/news/national/not-enough-data-on-kids-and-covid-vaccines-canadian-expert-cautions): *Not enough data on kids and COVID vaccines, Canadian expert cautions*

> A Canadian vaccine expert is voicing his concerns about the current push to get children vaccinated against COVID-19, noting that there is currently not enough data to conclude that the benefits outweigh the risks in younger age cohorts.

> “That’s not how this is supposed to work, our kids should not be the guinea pigs,” Bridle says.

> “Stop using our children as shields in this battle that is an adult battle.”





## Media Appearances with Covid Plan B

- 2021-02-24 [“I would probably prefer to have natural immunity” — Dr Byram Bridle (Viral Immunologist)](https://web.archive.org/web/20210426111812/https://dryburgh.com/byram-bridle-coronavirus-vaccine-concerns/)

> He gave this talk to the COVID Plan B group which opposes the official narrative of the deadliness of Covid-19 and its continuous new strains, the necessity of the lockdowns and the theory of elimination, and instead proposes treating the coronavirus like the seasonal flu (using vaccines).

- 2020-08-17 video at Covid Plan B's [COVID-19 Symposium 2020](https://www.youtube.com/watch?v=HndetYzK8gU)
- 2021-02-22 radio interview at [Magic](https://www.magic.co.nz/home/news/2021/02/byram-bridle--associate-professor-of-viral-immunology-at-the-uni.html)
- 2021-04-13 interview with Jim Desmond on [YouTube](https://www.youtube.com/watch?v=f1WLkbd6RZY)
- 2021-02-14 video at Covid Plan B's [COVID-19 Symposium 2021](https://www.youtube.com/watch?v=VrNQ8hkxHw8)
-
> at 7:53 "... the disease is caused in some poeple - certainly not all - the disease is caused by a virus known as SARS coronavirus two."





## Other Media

- https://theconversation.com/can-antibody-tests-tell-us-who-is-immune-to-covid-19-138240
- 2021-03-19 (TODO) [archive](https://web.archive.org/web/20210413195053/https://dryburgh.com/vanden-bossche-theory-fact-or-fiction/)





## Bridle Acts as an Expert Witness for Adamson Barbecue

The ~~Adamson~~ Dipshit Barbecue debacle can be reviewed elsewhere [1](https://www.cbc.ca/news/canada/toronto/adamson-bbq-locks-changed-1.5817090), [2](https://www.cbc.ca/news/canada/toronto/adamson-bbq-locks-changed-1.5817090),  [3](https://web.archive.org/web/20210531033405/https://www.rebelnews.com/free_adam_skelly).

The testimony, requested on behalf of [Elders Without Borders](https://elderswithoutborders.ca/), submitted 2021-04-10, is available [here](https://img1.wsimg.com/blobby/go/472827d6-3b15-4c33-834a-970e550df358/downloads/Affidavit%20of%20Expert%20Witness%20B.%20Bridle%20-%20Respon.pdf?ver=1620059730771). They provide additional court documents related to the Dipshit Barbecue debacle [here](https://elderswithoutborders.ca/legal-action#4f693f22-3555-4ac1-bdf0-0781ac9eef40).


### Contents

1. The Problem
2. Dr. Byram W. Bridle’s Credentials and Role in the COVID-19 Pandemic
3. SARS-CoV-2: A Virus that Follows Typical Population Dynamics
4. SARS-CoV-2 is Not a Problem of Pandemic Proportions
5. Results of PCR Tests to Detect SARS-CoV-2 Must be Interpreted with Caution
6. Asymptomatic Transmission of SARS-CoV-2 is Negligible
7. Individuals Who Had COVID-19 Cannot Re-Transmit the Virus.
8. SARS-CoV-2 Variants of Concern
9.  Masking Lacks Rationale in the Context of SARS-CoV-2 Spreading via Aerosols
10. Prolonged Isolation and Masking of Children Can Cause Irreparable Harm to Their Immune Systems
11. Early Treatment Options that Represent Reasonable Alternatives that Would Preclude the
Enactment of Emergency Orders and the Emergency Use Authorization of Experimental Vaccines


### Excerpts

#### 1. The Problem

> Rather than taking a balanced approach, in which economic, physical and human resources could be focused on protecting the most vulnerable, Ontario has opted for a very long-term ‘one-size-fits-all’ approach that has had dramatic consequences for the minority of high-risk individuals as well as low-risk people, who are in the majority. What follows is a discussion some of the data that highlight where COVID-19 policies have been flawed and/or have caused harm, which, in some cases, has been irreparable.

#### 3. SARS-CoV-2: A Virus that Follows Typical Population Dynamics

> For example, 2009 was a year in which deaths due to motor vehicle accidents were
relatively low; 569 Ontarians died. These types of annual deaths would also be preventable with the
implementation of stay-at-home orders. Further, many chronic fatal diseases (e.g. cancers, heart
disease, etc.) have been relatively neglected in favour of diverting resources to COVID-19 lockdown
measures. This will result in irreparable future harm in the form of increased death rates that have yet
to be determined. And this does not account for other deaths indirectly caused by COVID-19 policies,
including suicides due to increased mental health issues, etc. Indeed, the Ontario government needs to
determine if their current policies have placed a premium on lives lost due to COVID-19 over those lost
to other causes. Revising or revoking these policies could result in a net saving of lives in Ontario.

#### 4. SARS-CoV-2 is Not a Problem of Pandemic Proportions

>  Remarkably, as the data regarding total infections has become more accurate, the IFR for SARS-CoV-2 has dropped to only ~0.15%.

Cited for infection rate is *[Reconciling estimates of global spread and infection fatality rates of COVID-19: An overview of systematic evaluations](https://doi.org/10.1111/eci.13554)* by John Ioannidis.

> The IFR for SARS-CoV-2 was vastly overestimated at the beginning of the declared
pandemic. It is now approaching the range of a serious influenza outbreak, but with severity of disease
limited to a more restricted demographic (i.e. unlike influenza viruses, SARS-CoV-2 is not particularly
dangerous to the very young).

#### 5. Results of PCR Tests to Detect SARS-CoV-2 Must be Interpreted with Caution

> The conclusion is that it is erroneous to declare samples with high Ct values, especially those above 30, as being positive for infectious SARS-CoV-2. It was even concluded in a study by La Scola B, et al., concluded that patients testing ‘positive’ with Ct values above 33-34 could likely be discharged from hospitals17. This means that a very large but unknown number of positive cases reported in Ontario were likely not true positives.

#### 6. Asymptomatic Transmission of SARS-CoV-2 is Negligible

> Positive PCR tests for SARS-CoV-2 in asymptomatic people are often based on high Ct values, which, in
and of themselves, raise the question of whether these individual harbors infectious viral particles. The
low prevalence of positive RT-PCR tests in asymptomatic people often does not differ much from the
false positive rate. These issues combined with a functional cell-based assay to prove infectivity renders
results of asymptomatic testing nearly impossible to interpret accurately.

> Testing of asymptomatic people for the presence of portions of the SARS-CoV-2 genome
does not make medical nor economic sense. Positive test results cannot be interpreted in a clinically
meaningful way. Also, there is no substantial evidence to suggest that people who are asymptomatic
represent a substantial risk of causing COVID-19-related hospitalizations or deaths in others.

#### 7. Individuals Who Had COVID-19 Cannot Re-Transmit the Virus.

> The primary effectors against viruses in the adaptive arm of the immune system are cytotoxic T cells that can kill virally infected cells to prevent them from serving as a ‘virus-production factory’, and B cells, which can produce antibodies to neutralize the virus and prevent it from entering cells.

> Note that some non-immunologists have erroneously concluded that memory conferred by natural infection with SARS-CoV-2 is not long-lasting. However, this has been based on assessments that show declining concentrations of virus-specific antibodies. The antibodies are produced by B cells. The antibodies are merely proteins in circulation with limited half-lives. They will be cleared from circulation over time. The relevant measure of memory is detection of memory B and T cells.

#### 8. SARS-CoV-2 Variants of Concern

> The goal in Canada should not be to get everyone vaccinated per se. Instead, the goal should be to get as many Canadians immune to SARS-CoV-2 as possible. There are two ways to achieve this: 1. Vaccination, 2. Natural acquisition of immunity. The great news is that Canada might be closer to the natural acquisition of herd immunity than what was previously appreciated, likely due, in large part, to the ongoing spread of the virus after the implementation of ineffective masking and misguided physical distancing policies that failed to account for the physics behind aerosol-mediated transmission of SARS-CoV-2. Like many other viruses, including other coronaviruses and influenza viruses, SARS-CoV-2 will likely become endemic, meaning that we may encounter new versions of the virus on a regular and long-term basis. As such, it is imperative that we learn to live with SARS-CoV-2 rather than attempting to hide from it; just like we have done with the other respiratory pathogens that we have accepted as a trade-off for living our lives outside the confines of lockdowns.

His reference is *[A majority of uninfected adults show preexisting antibody reactivity against SARS-CoV-2](https://doi.org/10.1172/jci.insight.146316)* (Majdoubi et al).


#### 9.  Masking Lacks Rationale in the Context of SARS-CoV-2 Spreading via Aerosols

> SARS-CoV-2 is defined as what is known as a ‘containment level-3 pathogen’ by the Public Health Agency
of Canada.

> A person wearing a low-cost mask would not be allowed to enter a containment level-3 facility due to a
profound lack of protection. There is, therefore, a large discrepancy between what truly protects an
individual from SARS-CoV-2 and the public health messaging surrounding cloth and surgical masks,
which falsely implies a substantial amount of protection.

> There are potential harms associated with long-term masking. Not only do masks fail to efficiently stop
the spread of COVID-19-laden aerosols, in some cases they may cause harm. ... Long-term prevention of exposure to the microbial world and natural environment in children has been associated with an increased incidence of allergies, asthma and autoimmune diseases based on an immunological principle known as the ‘hygiene hypothesis’ (see section 10 for the details).

> Another potential harm of wearing masks is the psychological effect it has on adherence to public health protocols ...  Additional problems include things like blunting social cues by preventing reading of facial body language, muffling speech (a particular concern for individuals with pre-existing speech disorders) and preventing lip-reading.

> Once one realizes that SARS-CoV-2 can pass through low-cost masks and travel >2 meters and sometimes much further on ‘droplet nuclei’ in pulmonary aerosols, it becomes readily  apparent that the policies of mask-wearing and two-meter physical distancing are not adequately  protective against the spread of SARS-CoV-2. If low-cost masking combined with only two-meter  physical distancing does little to prevent the spread of SARS-CoV-2, it would be expected that a  relatively high proportion of Canadians would have naturally acquired immunity to the virus over the  past year. Indeed, this is precisely what was found in a recently published study that showed that the  majority of apparently healthy adults in British Columbia have evidence of naturally acquired immunity4

#### 10. Prolonged Isolation and Masking of Children Can Cause Irreparable Harm to Their Immune Systems

> Raising children during the pandemic has largely occurred in isolated and highly sanitized environments that are unprecedented in extent and duration. These kids are at greater risk of developing hypersensitivities and autoimmune diseases than anyone before them. The immune systems of children are not designed to develop in isolation from the microbial world. To minimize further lifelong damage to their immune systems, masking and isolation policies must be rescinded as soon as possible.


This is basically a re-hashing of the articles below.


#### 11. Early Treatment Options that Represent Reasonable Alternatives that Would Preclude the Enactment of Emergency Orders and the Emergency Use Authorization of Experimental Vaccines

> As a researcher with funding to develop COVID-19 vaccines, I have been monitoring developments with respect to early treatment options very closely. These include the use of hydroxychloroquine, vitamin D3, and ivermectin. Although each of these appear to be valid, safe, and effective treatment options, due to time constraints, I have focused over the past year on careful examinations of the growing body of literature but using ivermectin as an effective early treatment for COVID-19.

> Indeed, I have been very surprised that emergency use authorization of experimental vaccines has remained in place despite an avalanche of data that strongly supports the safe and effective use of ivermectin to treat COVID-19 when the drug is administered early in the disease presentation and is supervised by a physician.

>  Having reviewed the scientific literature, the conclusion can be drawn that the data is such that Canada should include ivermectin for early out-patient treatment for COVID-19, and as a prophylactic, while people are being vaccinated. As far as integrity goes, multiple clinical trials from different countries saying the same thing, that the treatment works, both in the early and late stages of the disease. Principal Investigators on these studies were acting in good faith with no financial interests by the institutions carrying them out.
