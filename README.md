

# Byram Bridle

## Contents

- [Byram Bridle](#byram-bridle)
  - [Contents](#contents)
  - [Bridle's History](#bridles-history)
  - [The Canadian Covid Care Alliance](#the-canadian-covid-care-alliance)


## Bridle's History

A [history of Bridle's claims](bridle-history.md) in the media.

## The Canadian Covid Care Alliance

An [overview and membership](canadian-covid-care-alliance.md) of the Canadian Covid Care Alliance (authors of various documents Bridle has released).