


# The Canadian Covid Care Alliance (CCCA)

Website: [canadiancovidcarealliance.org](https://web.archive.org/web/20210605022229/https://www.canadiancovidcarealliance.org/)


## Contents

- [The Canadian Covid Care Alliance (CCCA)](#the-canadian-covid-care-alliance-ccca)
  - [Contents](#contents)
  - [History of the Open Letter to the Ontario Premier](#history-of-the-open-letter-to-the-ontario-premier)
    - [The open letter was written by the Canadian Covid Care Alliance](#the-open-letter-was-written-by-the-canadian-covid-care-alliance)
    - [The Open Letter took inspiration from a letter to the Alberta Premier](#the-open-letter-took-inspiration-from-a-letter-to-the-alberta-premier)
    - [Related Links](#related-links)
  - [The Open Letter to the Ontario Premier](#the-open-letter-to-the-ontario-premier)
  - [Members of CCCA](#members-of-ccca)
    - [1) Howard Tenenbaum](#1-howard-tenenbaum)
      - [Misinformation Context](#misinformation-context)
    - [2) Bonnie Mallard](#2-bonnie-mallard)
      - [Misinformation Context](#misinformation-context-1)
    - [3) Byram Bridle](#3-byram-bridle)
      - [Misinformation Context](#misinformation-context-2)
    - [4) Michael Palmer](#4-michael-palmer)
      - [Misinformation Context](#misinformation-context-3)
    - [5) Dawn DeCunha](#5-dawn-decunha)
      - [Misinformation Context](#misinformation-context-4)
    - [6) Linda Rapson](#6-linda-rapson)
      - [Misinformation Context](#misinformation-context-5)
    - [7) Philip Oldfield](#7-philip-oldfield)
      - [Misinformation Context](#misinformation-context-6)
    - [8) Ira Bernstein](#8-ira-bernstein)
      - [Misinformation Context](#misinformation-context-7)
  - [Post-Interview E-mail Blast](#post-interview-e-mail-blast)
  - [Other Reference](#other-reference)
  - [As a Non-Profit](#as-a-non-profit)



## History of the Open Letter to the Ontario Premier

On or about April 19th, 2021, a group of 9 scientists wrote a letter requesting Doug Ford (the Premier of the province of Ontario), and his Chief Medical Officer of Health (CMOH) Dr. David Williams "*join a multidisciplinary team in an open academic discussion,*" and note "*This request for an open debate is similar to the one requested of the Premier of Alberta, Jason Kenney, and his Covid-19 Task Force on March 29, 2021.*" 

### The open letter was written by the Canadian Covid Care Alliance

On 2021-06-04, many of the authors of the open letter appeared in a [TrialSiteNews video](https://web.archive.org/web/20210605023347/https://trialsitenews.com/covid-19-expert-panel-the-path-forward-for-canadians-trialsite-webinar/), and make reference to the open letter. Additionally, in other videos Bridle references the same group of 9; it's safe to conclude the Canadian Covid Care Alliance penned the letter.

### The Open Letter took inspiration from a letter to the Alberta Premier

The Open Letter indicates the authors took inspiration from a similar letter sent to the Alberta Premier. The Albertian letter and the authoring group are described [here](https://web.archive.org/web/20210605014146/https://westernstandardonline.com/2021/03/emergency-group-challenges-kenney-hinshaw-to-a-debate-over-covid-restrictions/).

> The group consists of Lt.-CoL. (retired) David Redman, CD1, BEng, MSEE, Dr. Dennis Modry, BSc, MD, MSc, FRSC, FACCP, FACS, Dr. Roger Hodkinson, MA, MB, FRCPC, FCAP, and retired police officer David Dickson said the purpose of the debate is to provide Albertans with a transparent discussion of complete data to date and various strategies used over the last year, today, and in the future.

Not surprisingly, they are progenitors of COVID-19 misinformation (taken from the links below).

> Modry said he has thoroughly researched the most recent medical studies from around the world that show lockdowns don’t work, and that any lockdown must come at the very beginning when the virus has only been detected in one or two areas.

> The retired surgeon said he agrees with the Great Barrington Declaration – now signed by more than 50,000 medical experts around the world – that calls on governments to get society back to normal and then take steps to protect vulnerable groups like the elderly.

> Hodkinson says the virus is no worse than a “bad flu.”

### Related Links

End Canada Lockdown

- https://endcanadalockdown.com/
- https://web.archive.org/web/20210605020111/https://www.nomorelockdowns.ca/facts_vs_fears
- https://web.archive.org/web/20210605015620/https://endcanadalockdown.com/our-partners/
- https://web.archive.org/web/20210605015726/https://endcanadalockdown.com/experts-archive/

About Redman

- https://www.todayville.com/no-lockdowns-in-alberta-if-emergency-management-agency-was-in-charge-former-executive-director-david-redman/
- https://web.archive.org/web/20210605015039/https://edmontonjournal.com/opinion/columnists/opinion-lockdowns-are-the-wrong-response-to-covid-19
- https://www.youtube.com/watch?v=Cgm3Xjz6N7g


## The Open Letter to the Ontario Premier

The contents of the letter were published in the Epoch Times [here](https://web.archive.org/web/20210605012802/https://www.theepochtimes.com/ontario-scientists-ontario-premier-doug-ford-open-scientific-debate-challenge_3789862.html) and COVEXIT [here](https://web.archive.org/web/20210526135158/https://covexit.com/ontario-scientists-mds-urge-the-province-to-hold-an-open-scientific-debate/). The Epoch Times is a far-right media outlet, with ties to the Falun Gong religious movement, and COVEXIT is a general COVID-19 misinformation blog.   

The contents are reproduced below, with numbers added to the signatories for reference.

```
Open Letter

The following letter, signed by nine specialists, including medical doctors, health sciences professors, and other scientists from different universities and clinics in Ontario, was sent to Ontario Premier Doug Ford. The scientists are asking the premier and his pandemic response team to engage in a debate to challenge some of the response measures taken by the province.

Today we are making a formal request of the Ontario Premier, Doug Ford, and his pandemic response team, including the Chief Medical Officer of Health, Dr. David Williams, to join a multidisciplinary team in an open academic discussion, to be held publicly, about the events surrounding Ontario’s pandemic response over the last year and into the future.

This request for an open debate is similar to the one requested of the Premier of Alberta, Jason Kenney, and his Covid-19 Task Force on March 29, 2021. We believe the Canadian public across all provinces deserves to hear an open debate of all the facts so they can stay informed about the various points of view expressed by highly qualified Canadian scientists. These are experts in the fields of clinical medicine, biology, immunology, virology, epidemiology, and infectious diseases.

1) Dr. H. C. Tenenbaum (Professor of Laboratory Medicine and Pathobiology, Faculty of Medicine, University of Toronto)

2) Dr. B. A. Mallard (Professor of Immunogenetics, Department of Pathobiology, University of Guelph

3) Dr. B. W. Bridle (Associate Professor of Viral Immunology, Department of Pathobiology, University of Guelph)

4) Dr. M Palmer (Associate Professor of Biochemistry, University of Waterloo)

5)  Dr. P.E. Alexander (Assistant Professor of Health Research Methodology, Evidence-Based-Medicine, Department of Health Research, McMaster University)

6) Dr. D. DeCunha (Clinical and Chief Psychologist, Psychology Works, Toronto)

7) Dr. P. Oldfield (PhD, Independent Scientific/Regulatory Consultant, Fellow of the Royal Society of Chemistry)

8) Dr. L. Rapson (Assistant Professor Department of Family & Community Medicine U of Toronto)

9) Dr. I. Bernstein, (B.Sc., M.D.,C.C.F.P., F.C.F.P., Lecturer, Department of Family and Community Medicine, University of Toronto).

We are formally asking for a public forum with the Premier and his COVID-19 response team, in order to provide Ontarians with a transparent discussion of the complete data to date and the various strategies used over the year, today and in the future in relation to the Provincial response to the SARS CoV-2 pandemic. The event moderator(s) is to be agreed. All biographies will be made available upon request.

This event is being organized in direct response to ongoing societal lockdown and closure restrictions (including school closures), the sub-optimal vaccine roll-out, lack of treatment options, the handling of the virus in long-term care homes, hospitals, as well as with other vulnerable demographic groups, and the impact these have had on the province and the overall health and wellbeing of all Ontarians.

The topics we want to cover include, but are not limited to:

1. What are the actual dangers of SARS-CoV-2 by age group and steps taken to protect those in long-term care and the most vulnerable, age 60 and older with co-morbidities?

2. What is the rationale for the current vaccine roll-out and alteration to the manufacturers’ recommended vaccination protocols?

3. What controls are in place to limit the number of cycles used in performing PCR tests to 30 cycles to avoid undue false positives inflating the cases reported?

4. Identification and review of supporting science and corresponding data that led the province to their conclusions that the approach in use was, and is, appropriate, and not worth revision as the crisis evolved over the past 12+ months.

5. How effective are stringent population-wide restrictions (lockdowns, school closures, mask mandates, and stay at home orders) at controlling the pandemic and are they the best response given the collateral damage?

6. Why have early multidrug treatment measures, proven to be highly effective and including but not limited to ivermectin, hydroxychloroquine, doxycycline, azithromycin and other compounds used routinely in other countries, not being employed to save lives in Ontario, an action that would virtually eliminate the perceived need for lockdowns, school closures, masking among other things.

7. What are the priorities in the management of the pandemic and how can we shift the response from fear to confidence?

For over a year the Government of Ontario has used a blanket non-targeted and population-wide approach using measures that now appear to be, at best, arbitrary and worse capricious. These have led to unnecessary death of our seniors and collateral damage to our society’s mental health, deaths of despair, drug overdoses (intentional or not), alcoholism, suicidal ideation and worse.

The education of our children has been hurt beyond comprehension. Similarly, knowing that the school environment is often the 1st place where evidence for child abuse or certain illnesses are initially recognized the impact on the health of our children has been very damaging. The incidence of presentation to emergency departments of parents with injured children caused by child abuse has increased dramatically! And of course, the impact on our children’s education and development has been horrible.

Ontarians with severe illnesses, small businesses, and our economy have suffered immeasurable harms. In the interest of the 14.7 million people living in this Province, we want to provide all of them with a transparent discussion of the data, facts, and alternatives so we can all make an informed, collective decisions on how best to move forward with the interests of every demographic in mind.

PUBLIC FORUM DETAILS:

Where: Online event with details to be determine upon acceptance of the Ontario Government and the Chief Medical Officer of Health.

When: Within a week to two given the urgent nature of the situation in Ontario. We request that that a date be agreed upon between April 26 and May 7, 2021.
```

It's not clear if they got any response.



## Members of CCCA

### 1) Howard Tenenbaum

Howard Tenenbaum is a Professor at the Faculty of Dentistry, University of Toronto.

- [Link](https://web.archive.org/web/20210227034413/https://www.dentistry.utoronto.ca/faculty-profiles/howard-tenenbaum) to their University of Toronto Faculty of Dentistry profile page.
- [Link](https://web.archive.org/web/20210605012429/https://www.lmp.utoronto.ca/faculty/howard-tenenbaum) to their University of Toronto Laboratory Medicine and Pathobiology profile page.

#### Misinformation Context

- to be completed.




### 2) Bonnie Mallard

Bonnie Mallard is a professor at the Department of Pathobiology, University of Guelph.

- [Link](https://ovc.uoguelph.ca/pathobiology/people/faculty/Bonnie-Mallard) to their University of Guelph profile page.

#### Misinformation Context

- They joined [an anti-mask rally](https://www.guelphtoday.com/wellington-county/anti-mask-protesters-show-their-faces-at-minto-council-2716299) calling for "the end to the state of emergency."




### 3) Byram Bridle

Byram Bridle is a professor at the Department of Pathobiology, University of Guelph.

- [Link](https://ovc.uoguelph.ca/pathobiology/people/faculty/Byram-W-Bridle) to their University of Guelph profile page.


#### Misinformation Context

See [byrambridle.com](https://byrambridle.com).




### 4) Michael Palmer

Michael Palmer is a faculty member at the Department of Chemistry, University of Waterloo.

- [Link](https://web.archive.org/web/20210605012546/http://science.uwaterloo.ca/~mpalmer/) to their faculty site at the University of Waterloo.

#### Misinformation Context

- to be completed.




### 5) Dawn DeCunha

Dawn DeCunha is a clinical psychologist at Breakthrough Therapy and Psychology Works.

- [Link](https://ca.linkedin.com/in/dr-dawn-decunha-37a9b46) to their LinkedIn.
- [Link](https://web.archive.org/web/20210605024730/https://breakthroughtherapy.net/about-us/) to their profile at Breakthrough Therapy.
- [Link](https://web.archive.org/web/20210605012206/https://psychologyworks.net/about-us/) to their profile at Psychology Works.

#### Misinformation Context

- to be completed.




### 6) Linda Rapson

Linda Rapson is the medical director of Rapson Pain & Acupuncture Clinic.

- [Link](https://web.archive.org/web/20210605025506/https://www.rapsonpainclinic.com/team) to Rapson Pain & Acupuncture Clinic.
- [Link](https://doctors.cpso.on.ca/DoctorDetails/Rapson-Linda---Mary/0015142-19926) to CPSO.
- [Link](https://web.archive.org/web/20210605025130/https://www.uhnresearch.ca/researcher/linda-rapson) to her UHN page.

#### Misinformation Context

> She specializes in treating chronic pain by integrating acupuncture and nutrition with conventional medical approaches. [source](https://web.archive.org/web/20210605025506/https://www.rapsonpainclinic.com/team)

Their clinic offers:

- Acupuncture
- Traditional Chinese Medicine
- Electro-Acupuncture
- Osteopathy

They claim to have [successfully treated](https://web.archive.org/web/20210605033835/https://www.rapsonpainclinic.com/successful-treatments) among other things:

- phantom limb pain
- Crohn's disease
- allergic rhinitis
- asthma
- sinusitis
- substance use disorders (they may they refer this to a different acupuncturist)
- infertility (they refer this to a different acupuncturist)



### 7) Philip Oldfield

Philip Oldfield is a retired chemist.

- [Link](https://ca.linkedin.com/in/scrat) to their LinkedIn.
- [Link](https://web.archive.org/web/20210605011928/https://www1.ipage.com/green-certified/hosting-badge-11.png) to their consulting firm.

#### Misinformation Context

- to be completed.




###  8) Ira Bernstein

Ira Bernstein is a family physician.

- [Link](https://twitter.com/docira4health) to their Twitter.
- [Link](https://web.archive.org/web/20210605030638/https://doctorirabernstein.ourmd.ca/about-us) to their practice's About Us page.


#### Misinformation Context

> Dr. Bernstein recommends that patients of all ages optimize their cellular health with high quality nutritional supplements. ... Metabolic Balance, MB, is an all- natural health and weight management system, whose primary goal is to bring your body into balance with clean, whole foods based on the latest scientific findings. [source](https://web.archive.org/web/20210605041612/https://doctorirabernstein.ourmd.ca/nutritional-services-info)





## Post-Interview E-mail Blast

On 2021-05-31, following his appearance on Alex Pierson's radio show, Bridle sent out the [v120210530kl version of "Why Parents, Teens, and Children ...](assets/2021-05-31_v120210530kl_Why_Parents_Teens_and_Children.pdf) via [e-mail (BCC'd) to offer support for his claims](2021-05-31_bridle_email.md). The document was written by the *Canadian Covid Care Alliance* and was distributed with the file name "Guide to COVID-19 Vaccination for Parents". 

The authors provided a mailchimp link where readers could sign up for the *Canadian Covid Care Alliance* mailing list. The URL provided for the *Canadian Covid Care Alliance* ([canadiancovidcarealliance.org](https://web.archive.org/web/20210605022229/https://www.canadiancovidcarealliance.org/)) initially redirected users to the mailchimp mailing list signup page. Now, it directs to an [actual website](https://web.archive.org/web/20210605022229/https://www.canadiancovidcarealliance.org/). The domain was registered 2021-04-18. Therefore the group name was thought of prior to - but not referenced in - the original open letter.

The current version of the document on the website (as of 2021-06-07) is [v220210601 "Why Parents, Teens, and Children ...](assets/2021-XX-XX_v220210601_Why_Parents_Teens_and_Children.pdf).



## Other Reference

At "Covfefe Operations Intelligence", the CCCA was [listed with three members](https://web.archive.org/web/20210616172759/https://www.openontario.ca/corporations/2021/04/23/canadian-covid-care-alliance):

- David Ross
- Steven Pelech
- Karen Levins



https://www.mednet.ca/en/report/oma-district-11-physicians-lounge-virtual-sessio.html
https://vaccinechoicecanada.com/resources/covid-19/

https://www.canadahealthalliance.org/ivermectin.html


https://bird-group.org/who-are-bird/



## As a Non-Profit

https://www.ic.gc.ca/app/scr/cc/CorporationsCanada/fdrlCrpDtls.html?corpId=12955661

The registered office address is:

```
960 Lawrence Avenue West
Suite 306
Toronto ON M6A 3B6
Canada
```

This is Ira Bernstein's practice address. Also the address of https://laserdocs.ca/about-us/

