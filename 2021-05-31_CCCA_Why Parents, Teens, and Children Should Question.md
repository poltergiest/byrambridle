

# 2021-05-31 CCCA: Why Parents, Teens, and Children Should Question...

On 2021-05-31, Bridle sent [an e-mail](2021-05-31_bridle_email.md) to an undisclosed number of recipients; this is the contents of the document that was attached to that e-mail.

```
Why Parents, Teens, and Children Should Question the COVID-19 Vaccine 

There is no immediate threat of severe COVID-19 in the majority of Canadian children and adolescents. 

As of May 28, 2021, there have been 259,308 confirmed cases of SARS-CoV-2 infections in Canadians 19 years and under.  Of these, 0.048% were hospitalized, 0.06% were admitted to ICU, and 0.004% died1.  Seasonal influenza is associated with more severe illness than COVID-19.2

Pfizer BioNTech’s clinical data in children are limited and provide no information on rare but serious adverse effects or long-term safety as well as efficacy. 

Pfizer BioNTech’s study included 2,260 children and adolescents, 12-15 years of age, 1,131 of whom received the vaccine.  This is a very small number of adolescents and does not permit an evaluation of rare but serious side-effects, such as effects that may happen in only 1:5,000 adolescents.  Furthermore, with most of the adolescents followed for only 1 or 2 months after their 2nd dose, there is no data to support long-term safety. 

All of the COVID-19 vaccines in Canada are “Authorized under Interim Orders”.   

This means that continued use of the experimental vaccines is contingent on the collection of additional data from Pfizer BioNTech’s on-going study as well as other surveillance systems, including studies that Canadian adolescents are being invited to enroll in at the time of vaccination, to evaluate the safety and effectiveness of the vaccines. 

COVID-19 vaccines authorized for use in Canada result in production of virus spike protein. 

The Pfizer BioNTech vaccine is injected in a shoulder muscle. It was assumed that spike protein production takes place in white blood cells at this location, and then these cells present the spike protein on their surface so that a full immune response can take place. However, cells of the muscle and other organs also take up the vaccine. 

It was assumed that the spike proteins do not end up in circulation; however, this is being challenged by recent studies. 

Ogata et al., 20213 reported the detection of spike protein in the plasma of 3 of 13 young healthcare workers following vaccination with Moderna’s mRNA-1273 vaccine.  In one of the workers, the spike protein circulated for 29 days.  The data are limited and warrant further investigation for both the Moderna and Pfizer BioNTech COVID-19 vaccines. 

Recent studies indicate the spike protein, itself, may potentially be harmful.  

Recent studies4 suggest that the spike protein produced in response to vaccination, may bind and interact with various cells throughout the body, via their ACE2 receptors, potentially resulting in damage to various tissues and organs.  This risk, no matter how theoretical, must be investigated prior to the vaccination of children and adolescents. 

Health Canada authorized the COVID-19 vaccines without biodistribution and pharmacokinetic studies on the virus spike protein.  

Given the concerns about the spike protein, it is important that we fully understand: 
•	which cells are actually involved in the production of the spike protein, seeing that Pfizer’s own study submitted to the Japanese authorities shows the deposition of vaccine nanoparticles in various tissues and organs ; 
•	whether the spike protein is gaining access to the circulatory system and, if so, for how long; 
•	whether the spike protein crosses the blood-brain barrier; 
•	whether the spike protein interferes with semen production or ovulation,  • whether the spike protein crosses the placenta and impacts a developing baby, or  
•	whether the spike protein is excreted in the milk of lactating mothers.   
The same information is needed for the S1 subunit of the spike protein, which is the part that binds to ACE2 receptors; and which has also been detected in the plasma of individuals following mRNA-1273 (Moderna) vaccination (Ogata et al., 2021). 

The toxicity studies conducted with the Pfizer BioNTech vaccine do not allow for a safety assessment of the spike protein. 

Although Pfizer BioNTech conducted toxicity studies, including a reproductive toxicity study, they used rats as their animal model.  Although rats have ACE2 receptors, these receptors have a very low binding affinity for the spike protein.  In fact, of 14 mammalian species evaluated , ACE2 receptors of rats and mice had the lowest spike protein binding affinities, while ACE2 receptors in humans and rhesus monkeys had the highest.  So, while the current toxicity studies have provided useful information on the vaccine components, they provide little value in understanding the safety of the spike protein they code for. 

Where our children and adolescents are concerned, it is crucial that we carefully follow a precautionary principle. Children and adolescents have a miniscule risk of severe illness and death from COVID-19.  The risk of vaccination, no matter how theoretical, must be fully investigated and understood. 

Canadians must question the accelerated and indiscriminate vaccination of all children and adolescents with a vaccine for which critically important biodistribution, pharmacokinetic, and safety data on the SARS-CoV-2 spike protein are missing. 
```